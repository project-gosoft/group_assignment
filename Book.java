package Library;

import java.util.ArrayList;
import java.util.Scanner;

public class Book {

	public String title;
	public int idBook;

	static ArrayList<Book> book = new ArrayList<>();
	static Scanner input = new Scanner(System.in);

	public Book(String titleInput, int idBookInput) {
		this.idBook = idBookInput;
		this.title = titleInput;
	}

	public static void addItemBook(ArrayList<Book> book) {
		// create list book
		Book book1 = new Book("Herry Potter", 1);
		Book book2 = new Book("Herry Potter part2", 2);
		Book book3 = new Book("Herry Potter part3", 3);

		// add book to arrayList
		book.add(book1);
		book.add(book2);
		book.add(book3);
	}

	public static void listBook(ArrayList<Book> book) {
		if(book.size() == 0) {
			addItemBook(book);
		}
		System.out.println("");
		System.out.println("        List book");
		System.out.println("");
		System.out.println(" ID           Title");
		for (Book listBook : book) {
			System.out.println("" + listBook);
		}
	}

	public static void editBook(ArrayList<Book> book) {
		System.out.println("");
		if(book.size() == 0) {
			addItemBook(book);
		}

		for (Book listBook2 : book) {
			System.out.println("" + listBook2);
		}
		System.out.print("Please input your idBook want to edit :");
		int idBook;
		//การเช็คค่า Idbook ที่ input เข้ามา หากรับค่าเป็น String จะไปเข้าที่ catch
		boolean Program = false;
		while (Program == false)
			try {
				idBook = Integer.parseInt(input.next());
				System.out.print("please input new title want to edit :  ");
				String nameBook = input.next();
				book.set(idBook - 1, new Book(nameBook, idBook));
				System.out.println("");
				System.out.println(" ID           Title");
				for (Book listBook2 : book) {
					System.out.println("" + listBook2);
				}
				break;
			} catch (NumberFormatException nfe) {
				System.out.print("Please input idBook try again: ");
			}
		Program = true;
		

	}

	public static void deleteBook(ArrayList<Book> book) {
		if(book.size() == 0) {
			addItemBook(book);
		}
		System.out.println("");
		System.out.println(" ID           Title");

		for (Book listBook2 : book) {
			System.out.println("" + listBook2);
		}

		System.out.print("Please input your idBook want to delete :");
		int yourIdBook;
		//การเช็คค่า yourIdBook ที่ input เข้ามา หากรับค่าเป็น String จะไปเข้าที่ catch
		boolean Program = false;
		while (Program == false)
			try {
				yourIdBook = Integer.parseInt(input.next());
				book.remove(yourIdBook - 1);
				break;
			} catch (NumberFormatException nfe) {
				System.out.print("Please input idBook try again: ");
			}

		System.out.println("");
		System.out.println("Successful!!");
		
		Program = true;
	}

	public static void addBook(ArrayList<Book> book) {
		if(book.size() == 0) {
			addItemBook(book);
		}

		for (Book listBook2 : book) {
			System.out.println("" + listBook2);
		}
		System.out.println("");
		System.out.print("Please input your id Book want to add : ");
		int idBookToAdd;
		boolean Program = false;
		//การเช็คค่า idBookToAdd ที่ input เข้ามา หากรับค่าเป็น String จะไปเข้าที่ catch
		while (Program == false)
			try {
				idBookToAdd = Integer.parseInt(input.nextLine());
				boolean has = false;

				for (Book id : book) {
					while (id.idBook == idBookToAdd) {
						System.out.println("Name Sum No ADD !!! ");
						System.out.println("");
						System.out.print("Please input your id Book want to add : ");
						idBookToAdd = input.nextInt();
					}
				}
				
				if (has == false) {
					System.out.println("");
					System.out.print("Please input your name book want to add : ");
					String nameBook = input.next();
					Book addBook = new Book(nameBook, idBookToAdd);
					book.add(addBook);
					listBook(book);
				}
				break;
			} catch (NumberFormatException nfe) {
				System.out.print("Please input idBook try again: ");
			}
	}

	public static void exitProgram() {
		System.out.println("");
		System.out.println("Thank you for using library system. ");
	}

	@Override
	public String toString() {
		return " ID book :" + idBook + " | Title : " + title;
	}
}
